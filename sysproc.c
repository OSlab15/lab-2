#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}


//return parent id of current process

int
sys_get_parent_id(void)
{
  return myproc()->parent->pid;
}

int
sys_get_children(void)
{
  int childrenIds = 0, parentId;

  if(argint(0, &parentId) < 0)
    return -1;

  childrenIds = get_children_from_ptable(parentId);
  return childrenIds;
}

int
sys_get_descendant(void)
{
  int parentId;

  if(argint(0, &parentId) < 0)
    return -1;

  return get_descendant_from_ptable(parentId);
}

int
sys_nap(void)
{
  int n;
  uint ticks0;
  struct proc *p = myproc();

  if(argint(0, &n) < 0)
    return -1;
  n = n * 100;
 
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(p->killed){
      release(&tickslock);
      return -1;
    }
    release(&tickslock);
    acquire(&tickslock);
  }
  release(&tickslock);  

  return 0;
}

int
sys_count_num_of_digits(void)
{ 
  int num, digitsNum = 0, esiValBeforeChange;
  asm("movl %%esi, %0;"
      :"=r"(esiValBeforeChange)
      :
      :"%esi"
      );

  asm("movl %%esi, %0;"
      :"=r"(num)
      :
      :"%esi"
      );
  cprintf("value of esi register is %d\n", num);
  while(num != 0)
  {
    num = num / 10;
    digitsNum++;
  }
  cprintf("number of digits are %d\n", digitsNum);

  asm ("movl %0, %%esi;" 
      :      
      :"r"(esiValBeforeChange)
      :"%esi"
      );

  return digitsNum;
}

int
sys_get_time(void)
{
  struct rtcdate time;
  cmostime(&time);
  cprintf("time of system is: %d:%d:%d\n", time.hour, time.minute, time.second);
  return 0;
}