#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define STANDARD_INPUT 0
#define STANDARD_OUTPUT 1
#define DESCENDANT_NUM 6


int test_get_descendant(int parentId)
{
    return get_descendant(parentId);
}

void forkDescendant(int firstParPid)
{
    if(fork() == 0)
    {
        if(fork() == 0)
        {
            if(fork() == 0)
            {
                printf(STANDARD_OUTPUT, "syscall result = %d\n", get_descendant(firstParPid));
                printf(STANDARD_OUTPUT, "actual descendant are:\n%d\n", getpid());
            }
            else
            {
                wait();
                printf(STANDARD_OUTPUT, "%d\n", getpid());
            }
            
        }
        else
        {
            wait();
            printf(STANDARD_OUTPUT, "%d\n", getpid());
        }   
    }
    else
    {
        wait();
    }
}

int main()
{
    int firstParentId = getpid();
    
    forkDescendant(firstParentId);
    
    exit();
    
    return 0;
}