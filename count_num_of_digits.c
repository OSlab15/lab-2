#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(int argc, char const *argv[])
{
	if(argc < 2)
	{printf(1, "there is no number to count\n"); exit();}
	
    int num = atoi(argv[1]);
    int esiValBeforeSyscal;
    asm ("movl %%esi, %0;" 
         :"=r"(esiValBeforeSyscal)
         :
         :"%esi"
         );

    asm ("movl %0, %%esi;" 
         :      
         :"r"(num)
         :"%esi"      
         );    

    count_num_of_digits(num);

    asm ("movl %0, %%esi;" 
         :      
         :"r"(esiValBeforeSyscal)
         :"%esi"
         );

    exit();
    return 0;
}