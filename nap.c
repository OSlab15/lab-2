#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define STANDARD_OUTPUT 1

int main(int argc, char const *argv[])
{
    if(argc < 2)
    {
        printf(STANDARD_OUTPUT, "how many seconds?\n");
        return -1;
    }

    int sleepTime = atoi(argv[1]);
    get_time();
    nap(sleepTime);
    get_time();
    exit();
    return 0;
}